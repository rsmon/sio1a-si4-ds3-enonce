package programmes;

import static leclubdejudo.ClubJudo.listeDesMembres;
import static leclubdejudo.ClubJudo.ageMembre;
import static leclubdejudo.ClubJudo.categorieMembre;
import leclubdejudo.Membre;
import static utilitaires.UtilDate.aujourdhuiChaine;
import static utilitaires.UtilDate.convDateVersChaine;
import static leclubdejudo.Tris.trierParNomPrenom;

public class Exemple02 {

  public void executer() {
        
    afficherTitre();   
    
    trierParNomPrenom(listeDesMembres);
    
    for( Membre membre : listeDesMembres ){ traiterMembre( membre ); } 
    
    System.out.println();  
   }
  
   void afficherTitre() { System.out.printf("\n Liste des membres du Club agés de 22 ans au: %10s\n\n",aujourdhuiChaine()); }
 
    void traiterMembre(Membre pMembre) {
      
        if( ageMembre(pMembre) == 22) { 
        
           String categMembre       = categorieMembre(pMembre);      
           String dateNaiss         = convDateVersChaine(pMembre.dateNaiss); 
   
           System.out.printf(" %-10s %-10s %-10s %-20s\n", pMembre.prenom, pMembre.nom, dateNaiss, categMembre );
        }
    }
}


