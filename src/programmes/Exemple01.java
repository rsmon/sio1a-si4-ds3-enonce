package programmes;

import static leclubdejudo.ClubJudo.listeDesMembres;
import leclubdejudo.Membre;
import static utilitaires.UtilDate.convDateVersChaine;
import static utilitaires.UtilDate.aujourdhuiChaine;
import static leclubdejudo.Tris.trierParNomPrenom;

public class Exemple01 {

  public void executer() {
        
    afficherTitre();
        
    trierParNomPrenom( listeDesMembres );
    
    for ( Membre membre : listeDesMembres ) { traiterMembre( membre ); } 
    
    System.out.println();   
  }
 
  void afficherTitre() { System.out.printf("\n Liste des membres du Club le %-8s\n\n", aujourdhuiChaine()); }
  
  void traiterMembre( Membre pMembre ) {
     
        System.out.printf( " %-10s %-10s %-2s %-10s %4d kg %-20s %2d Victoires\n",
                
          pMembre.nom,
          pMembre.prenom,
          pMembre.sexe,
          convDateVersChaine(pMembre.dateNaiss),
          pMembre.poids,
          pMembre.ville,
          pMembre.nbVictoires
        );
   }
}
