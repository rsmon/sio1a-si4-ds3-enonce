package leclubdejudo;

import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.convChaineVersDate;


public class ClubJudo {
    
   public static List<Membre> listeDesMembres;  
   
   public  static  String[] lesCategories = { "super-légers","mi-légers","légers",
                                              "mi-moyens","moyens",
                                              "mi-lourd","lourds"
                                            };
   
   public static String  determineCategorie(String sexe, int poids){
     
       return lesCategories[determineIndiceCategorie(sexe,poids)];       
   }  
   
   public static int     determineIndiceCategorie(String sexe, int poids){

      return (  sexe.equals("M") )? chercheIndice( poids, limitesHommes ) : chercheIndice( poids, limitesFemmes );
   }  
   
   public static  void   afficherCategoriesPourSexe(String pSexe){
         
       System.out.printf("%-15s Jusqu'à      %3d  kg\n", lesCategories[0], limite(pSexe, 0));
        
       for (int i=1; i< lesCategories.length-1;i++){
                            
           System.out.printf("%-15s de  %d  kg à %3d  kg\n",lesCategories[i], limite(pSexe, i-1), limite(pSexe, i));     
       }
      
       int    indiceMax = lesCategories.length-1;
       System.out.printf("%-15s A partir de  %3d  kg\n", lesCategories[indiceMax],limite(pSexe,indiceMax-1)  
       );
   }
   
    
   public static int     ageMembre(Membre pMembre){ return ageEnAnnees(pMembre.dateNaiss);}
   
   
   public static String  categorieMembre( Membre pMembre ){ return determineCategorie(pMembre.sexe, pMembre.poids); }
   
   
   public static int  indiceCategorieMembre( Membre pMembre){ return determineIndiceCategorie(pMembre.sexe,pMembre.poids);}
   
   //<editor-fold defaultstate="collapsed" desc="CODE CREANT ET REMPLISSANT LA LISTE ListeDesMembres ">
  
  static {
      
      listeDesMembres= new LinkedList<Membre>();  
      
      Membre p1= new Membre();
      p1.nom="Durant";
      p1.prenom="Pierre";
      p1.sexe="M";
      p1.poids=83;
      p1.dateNaiss=convChaineVersDate("12/05/1993");
      p1.ville="Arras";
      p1.nbVictoires=9;
      
      Membre p2= new Membre();
      p2.nom="Martin";
      p2.prenom="Sophie";
      p2.sexe="F";
      p2.poids=52;
      p2.dateNaiss=convChaineVersDate("25/11/1991");
      p2.ville="Lens";
      p2.nbVictoires=6;
      
      Membre p3= new Membre();
      p3.nom="Lecoutre";
      p3.prenom="Thierry";
      p3.sexe="M";
      p3.poids=72;
      p3.dateNaiss=convChaineVersDate("05/08/1992");
      p3.ville="Arras";
      p3.nbVictoires=5;
      
      Membre p4= new Membre();
      p4.nom="Duchemin";
      p4.prenom="Fabienne";
      p4.sexe="F";
      p4.poids=61;
      p4.dateNaiss=convChaineVersDate("14/3/1992");
      p4.ville="Lens";
      p4.nbVictoires=10;
      
      Membre p5= new Membre();
      p5.nom="Duchateau";
      p5.prenom="Jacques";
      p5.sexe="M";
      p5.poids=91;
      p5.dateNaiss=convChaineVersDate("18/07/1992");
      p5.ville="Bapaume";
      p5.nbVictoires=4;
      
      Membre p6= new Membre();
      p6.nom="Lemortier";
      p6.prenom="Laurent";
      p6.sexe="M";
      p6.poids=76;
      p6.dateNaiss=convChaineVersDate("18/02/1989");
      p6.ville="Arras";
      p6.nbVictoires=12;
      
      Membre p7= new Membre();
      p7.nom="Dessailles";
      p7.prenom="Sabine";
      p7.sexe="F";
      p7.poids=68;
      p7.dateNaiss=convChaineVersDate("23/03/1990");
      p7.ville="Arras";
      p7.nbVictoires=3;
      
      Membre p8= new Membre();
      p8.nom="Bataille";
      p8.prenom="Boris";
      p8.sexe="M";
      p8.poids=102;
      p8.dateNaiss=convChaineVersDate("17/11/1991");
      p8.ville="Vitry-En-Artois";
      p8.nbVictoires=7;
      
      Membre p9= new Membre();
      p9.nom="Lerouge";
      p9.prenom="Laëtitia";
      p9.sexe="F";
      p9.poids=46;
      p9.dateNaiss=convChaineVersDate("09/10/1992");
      p9.ville="Lens";
      p9.nbVictoires=4;
      
      
      Membre p10= new Membre();
      p10.nom="Renard";
      p10.prenom="Paul";
      p10.sexe="M";
      p10.poids=68;
      p10.dateNaiss=convChaineVersDate("16/08/1992");
      p10.ville="Lens";
      p10.nbVictoires=9;
      
      Membre p11= new Membre();
      p11.nom="Durant";
      p11.prenom="Jacques";
      p11.sexe="M";
      p11.poids=75;
      p11.dateNaiss=convChaineVersDate("13/04/1990");
      p11.ville="Arras";
      p11.nbVictoires=4;
      
      Membre p12= new Membre();
      p12.nom="Delespaul";
      p12.prenom="Martine";
      p12.sexe="F";
      p12.poids=55;
      p12.dateNaiss=convChaineVersDate("25/02/1991");
      p12.ville="Lens";
      p12.nbVictoires=6;
      
      
      ajouterLesMembresALaListe(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12);
      
  }
  
  static void ajouterLesMembresALaListe(Membre p1, Membre p2, Membre p3, Membre p4, Membre p5, Membre p6, Membre p7, Membre p8, Membre p9, Membre p10, Membre p11, Membre p12) {
      
      listeDesMembres.add(p1); listeDesMembres.add(p2);  listeDesMembres.add(p3); 
      listeDesMembres.add(p4); listeDesMembres.add( p5); listeDesMembres.add( p6);
      listeDesMembres.add(p7); listeDesMembres.add(p8);  listeDesMembres.add(p9); 
      listeDesMembres.add(p10); listeDesMembres.add(p11);listeDesMembres.add(p12);
  }
  //</editor-fold>
   
   //<editor-fold defaultstate="collapsed" desc="RESTE DU CODE">  
   
   private static int    chercheIndice(int poids, int[] tableauLimites ){
   
      int i;
      
      for(i=0;i<tableauLimites.length;i++)
      {         
         if( poids<tableauLimites[i] )
         {  
            break;
         }
      }
   
      return i;
   }  
   
  
    private static int limite(String pSexe, int indice) {
        return (pSexe.equals("M")) ? limitesHommes[indice]-1:limitesFemmes[indice]-1;
    }
   
   static  int[]    limitesHommes = {60, 66, 73, 81, 90, 100};
   static  int[]    limitesFemmes = {48, 52, 57, 63, 70, 78};
   

  

  
  
     //</editor-fold>

}








