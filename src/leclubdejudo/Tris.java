package leclubdejudo;

import static java.util.Collections.reverse;
import static java.util.Collections.sort;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Tris {
    
    public static void  trierParNomPrenom(List<Membre> lp){
          sort(lp, new ComparateurParNomPrenom());
    }
    
    public static  void trierParSexeNomPrenom(List<Membre> lp){
          sort(lp, new ComparateurParSexeNomPrenom());
    }
    
    public static  void trierParPoids(List<Membre> lp){
          sort(lp, new ComparateurParPoids());
    }
    
    public static  void trierParNom(List<Membre> lp){
          sort(lp, new ComparateurParNom());
    }
    
    public static  void trierParNbVictoiresDecroissant(List<Membre> lp){
          
          sort(lp, new ComparateurParNbVictoiresDecroissant());
          reverse(lp);
    }  

    public static List<Membre> teteDeListe(List<Membre> liste, int nb){
      
      List<Membre> resultat= new LinkedList<Membre>(); 
      int i=0;
      for( Membre elem : liste){
          resultat.add(elem);
          i++;
          if( i==nb) break;
      }
      return resultat;
    
    }
    
    public static class  ComparateurParNomPrenom implements Comparator{

      @Override
      public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Membre p  = (Membre)  t ;
        Membre p1 = (Membre)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom/prénom">
            
            resultat=  p.nom.compareTo(p1.nom);
            if (resultat==0) resultat= p.prenom.compareTo(p1.prenom);
            
        //</editor-fold>
         
        return resultat;  
      }    
    }
    
    public static  class ComparateurParNom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0; 
        
         Membre p  = (Membre)  t ;
         Membre p1 = (Membre)  t1;
            
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom">
            
            resultat=  p.nom.compareTo(p1.nom);
            
            //</editor-fold>
        
       
        
        return resultat;  
    }    
}
   
    public static  class ComparateurParSexeNomPrenom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Membre p  = (Membre)  t ;
        Membre p1 = (Membre)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par sexe/nom/prénom">
           
        resultat= p.sexe.compareTo(p1.sexe);
        if ( resultat==0){
             resultat=  p.nom.compareTo(p1.nom);
             if (resultat==0) resultat= p.prenom.compareTo(p1.prenom);
         }            
        
        //</editor-fold>
      
        return resultat;  
    }    
}
   
    public static class  ComparateurParPoids implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Membre p  =  (Membre)   t ;
          Membre p1 = (Membre)  t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par poids">
        
           if        ( p.poids   > p1.poids )   resultat =   1;
           else if  ( p.poids   < p1.poids )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
    }    
}
 
    public static class  ComparateurParNbVictoiresDecroissant implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Membre p  = (Membre)   t ;
          Membre p1 = (Membre)   t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par victoires">
        
           if       ( p.nbVictoires  > p1.nbVictoires)    resultat =   1;
           else if  ( p.nbVictoires  < p1.nbVictoires )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
    }    
}
}


